PROXY_RU = (3, "isp2.hydraproxy.com", 9989, True,
            "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Russia")
PROXY_US = (3, "isp2.hydraproxy.com", 9989, True,
            "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-UnitedStates")
PROXY_CAMEROON = (3, "isp2.hydraproxy.com", 9989, True,
                  "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Cameroon")
PROXY_ETHIOPIA = (3, "isp2.hydraproxy.com", 9989, True,
                  "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Ethiopia")
PROXY_INDONESIA = (3, "isp2.hydraproxy.com", 9989, True,
                   "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Indonesia")
PROXY_MEXICO = (3, "isp2.hydraproxy.com", 9989, True,
                   "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Mexico")
PROXY_Philippines = (3, "isp2.hydraproxy.com", 9989, True,
                     "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Philippines")
PROXY_BELGIUM = (3, "isp2.hydraproxy.com", 9989, True,
                 "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Belgium")
PROXY_MOLDOVA = (3, "isp2.hydraproxy.com", 9989, True,
                 "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Moldova")
PROXY_BANGLADESH = (3, "isp2.hydraproxy.com", 9989, True,
                    "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Bangladesh")
PROXY_Tunisia = (3, "isp2.hydraproxy.com", 9989, True,
                 "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Tunisia")
PROXY_Singapore = (3, "isp2.hydraproxy.com", 9989, True,
                   "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Singapore")
PROXY_Morocco = (3, "isp2.hydraproxy.com", 9989, True,
                 "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Morocco")
PROXY_Jamaica = (3, "isp2.hydraproxy.com", 9989, True,
                 "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Jamaica")
PROXY_Mongolia = (3, "isp2.hydraproxy.com", 9989, True,
                  "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Mongolia")
PROXY_India = (3, "isp2.hydraproxy.com", 9989, True,
                  "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-India")


def get_proxy(phone):
    if phone[0:3] == "237":
        return PROXY_CAMEROON
    elif phone[0:3] == "216":
        return PROXY_Tunisia
    elif phone[0:3] == "659":
        return PROXY_Singapore
    elif phone[0:3] == "143":
        return PROXY_Morocco
    elif phone[0:3] == "187":
        return PROXY_Jamaica
    elif phone[0:3] == "976":
        return PROXY_Mongolia
    elif phone[0:3] == "251":
        return PROXY_ETHIOPIA
    elif phone[0:3] == "91" and len(phone) == 12:
        return PROXY_India
    elif phone[0:2] == "62":
        return PROXY_INDONESIA
    elif phone[0:2] == "52":
        return PROXY_MEXICO
    elif phone[0:2] == "63":
        return PROXY_Philippines
    elif phone[0:2] == "22":
        return PROXY_BELGIUM
    elif phone[0:2] == "23":
        return PROXY_MOLDOVA
    elif phone[0:1] == "8":
        return PROXY_BANGLADESH
    elif phone[0] == "7" or phone[:1] == "+7" or phone[0] == "9" or phone[:1] == "+9":
        return PROXY_RU


    return PROXY_US
