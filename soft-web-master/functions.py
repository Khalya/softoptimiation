def do_logging(lines):
    with open("logs.txt", "a") as logs_file:
        logs_file.writelines([line + '\n' for line in lines])
# {"1": {"number": "919344731041", "timestamp": 1, "event": "join", "host": "Marzha St"}}
