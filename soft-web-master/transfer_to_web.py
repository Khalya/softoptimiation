import json
import os

from process import process_account, prepare_host


def distribute_sessions():
    try:
        os.mkdir("hosts")
    except:
        pass
    with open("db.json", "r") as dbfile:
        data = json.load(dbfile)

    for user in data["users"].values():
        try:
            os.mkdir(f"hosts/{user['host']}")
        except:
            pass
        try:
            os.system(
                f"cp 'sessions1/{user['account']}.session' 'hosts/{user['host']}/{user['account']}.session'")
            print(user["host"], user["account"])
        except:
            print(user["host"], user["account"])


def get_names(filename: str):
    names = []
    with open(filename, "r") as names_file:
        for line in names_file.readlines():
            print(line)
            username, full_name, bio = line.strip('\n').split('+::+')
            names.append((username, full_name, bio))
    return names


def clear_accounts(host, group_public_link, group_private_link, main_channel_public_link, main_channel_private_link,
                   mirror_channel_public_link, mirror_channel_private_link, main_channel_id, mirror_channel_id, names_filepath=None):
    prepare_host(host)
    phones = [file[:-8]
              for file in os.listdir(f"hosts/{host}") if file[-8:] == ".session"]

    if names_filepath:
        names = get_names(names_filepath)

    for idx, phone in enumerate(phones):
        if not names_filepath:
            process_account(phone, host, group_public_link, group_private_link, main_channel_public_link,
                            main_channel_private_link, mirror_channel_public_link, mirror_channel_private_link, main_channel_id, mirror_channel_id)
        else:
            process_account(phone, host, group_public_link, group_private_link, main_channel_public_link,
                            main_channel_private_link, mirror_channel_public_link, mirror_channel_private_link, main_channel_id, mirror_channel_id, True, *names[idx])
