import firebase_admin
from firebase_admin import db

from functions import do_logging

CREDS_PATH = "firebase_creds.json"
DATABASE_URL = (
    "https://blaze-trade-default-rtdb.asia-southeast1.firebasedatabase.app/"
)


class Database:
    def __init__(self, host):
        """
        Connect a Firebase account and create references for database parts.
        """
        cred_obj = firebase_admin.credentials.Certificate(CREDS_PATH)
        firebase_admin.initialize_app(cred_obj, {"databaseURL": DATABASE_URL})

        self.events = db.reference("/events")
        self.channels = db.reference("/channels")
        self.users = db.reference("/users")
        self.host = host

    def add_events(self, events, event_name):
        """
        Add events to the database.
        """
        for event in events:
            do_logging(
                [f"ADDED NEXT ONLINE EVENT FOR {event[0]} AT {event[1]}"])
            event_data = {
                "account": event[0],
                "host": self.host,
                "timestamp": event[1],
                "event": event_name,
            }
            print(event_data)
            self.events.push(event_data)

    def pop_events(self, time_threshold):
        """
        Pop events up to a certain time threshold from the database.
        """
        events = []
        # print(self.events.order_by_child("timestamp").get().items(), 'eef')
        for event_id, event in (
            # self.events.order_by_child("timestamp").end_at(
            #     time_threshold).get().items()
            self.events.order_by_child("timestamp").get().items()
        ):
            if event["host"] == self.host:
                events.append(event)
                self.events.child(event_id).set({})

        return events

    def get_channels(self, nickname):
        """
        Return a list of channels belonging to a certain admin.
        """
        if self.channels.get() is None:
            return []

        channel_names = set()

        for value in self.admins.get().values():
            if value["nickname"] == nickname:
                channel_names = set(
                    value["channels"] if "channels" in value else [])

        channels_data = []

        for value in self.channels.get().values():
            if value["name"] in channel_names:
                channels_data.append(
                    (value["name"], len(value["users"])
                     if "users" in value else 0)
                )

        return list(set(channels_data))

    def add_user_to_channel(self, channel_link, user):
        """
        Add the given user to the list of users of the given channel.
        """
        host = self.host
        for key, value in self.channels.get().items():
            if value["name"] == channel_link and value["host"] == host:
                value["users"] = (
                    value["users"] if "users" in value else []) + [user]
                self.channels.child(key).set(value)

    def remove_user_from_channel(self, channel_link, user):
        """
        Remove the given user from the list of users of the given channel.
        """
        host = self.host
        for key, value in self.channels.get().items():
            if value["name"] == channel_link and value["host"] == host:
                if "users" in value:
                    value["users"].remove(user)
                self.channels.child(key).update({
                    "users": value["users"] if "users" in value else []
                })

    def remove_user(self, phone):
        """
        Remove user from the database.
        """
        for key, val in self.users.get().items():
            if val["account"] == phone:
                self.users.child(key).set({})

        for key, val in self.events.get().items():
            if val["account"] == phone:
                self.events.child(key).set({})

        print(do_logging(f"DELETED ACCOUNT: {phone}"))

    def add_account(self, account, host):
        """
        Add a user to the database.
        """
        self.users.push(
            {
                "account": account,
                "host": host
            }
        )
