from telethon.tl.functions.channels import JoinChannelRequest
from telethon.tl.functions.account import UpdateProfileRequest, UpdateUsernameRequest
from telethon.tl.functions.photos import DeletePhotosRequest
from telethon.sync import TelegramClient
import time
import os
from time import sleep
import random
import json


API_ID = 2965455
API_HASH = "3a82ff0f95a0550589e4d16ff999764b"

PROXY_RU = (3, "isp2.hydraproxy.com", 9989, True,
            "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Russia")
PROXY_US = (3, "isp2.hydraproxy.com", 9989, True,
            "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-UnitedStates")
PROXY_CAMEROON = (3, "isp2.hydraproxy.com", 9989, True,
                  "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Cameroon")
PROXY_ETHIOPIA = (3, "isp2.hydraproxy.com", 9989, True,
                  "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Ethiopia")
PROXY_INDONESIA = (3, "isp2.hydraproxy.com", 9989, True,
                   "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Indonesia")
PROXY_MEXICO = (3, "isp2.hydraproxy.com", 9989, True,
                   "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Mexico")
PROXY_Philippines = (3, "isp2.hydraproxy.com", 9989, True,
                     "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Philippines")
PROXY_BELGIUM = (3, "isp2.hydraproxy.com", 9989, True,
                 "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Belgium")
PROXY_MOLDOVA = (3, "isp2.hydraproxy.com", 9989, True,
                 "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Moldova")
PROXY_BANGLADESH = (3, "isp2.hydraproxy.com", 9989, True,
                    "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Bangladesh")
PROXY_Tunisia = (3, "isp2.hydraproxy.com", 9989, True,
                 "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Tunisia")
PROXY_Singapore = (3, "isp2.hydraproxy.com", 9989, True,
                   "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Singapore")
PROXY_Morocco = (3, "isp2.hydraproxy.com", 9989, True,
                 "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Morocco")
PROXY_Jamaica = (3, "isp2.hydraproxy.com", 9989, True,
                 "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Jamaica")
PROXY_Mongolia = (3, "isp2.hydraproxy.com", 9989, True,
                  "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-Mongolia")
PROXY_India = (3, "isp2.hydraproxy.com", 9989, True,
                  "artm13933vwfh32450", "4FWJ1eAP30tUiTbo_country-India")


def get_proxy(phone):
    if phone[0:3] == "237":
        return PROXY_CAMEROON
    elif phone[0:3] == "216":
        return PROXY_Tunisia
    elif phone[0:3] == "659":
        return PROXY_Singapore
    elif phone[0:3] == "143":
        return PROXY_Morocco
    elif phone[0:3] == "187":
        return PROXY_Jamaica
    elif phone[0:3] == "976":
        return PROXY_Mongolia
    elif phone[0:3] == "251":
        return PROXY_ETHIOPIA
    elif phone[0:3] == "91" and len(phone) == 12:
        return PROXY_India
    elif phone[0:2] == "62":
        return PROXY_INDONESIA
    elif phone[0:2] == "52":
        return PROXY_MEXICO
    elif phone[0:2] == "63":
        return PROXY_Philippines
    elif phone[0:2] == "22":
        return PROXY_BELGIUM
    elif phone[0:2] == "23":
        return PROXY_MOLDOVA
    elif phone[0:1] == "8":
        return PROXY_BANGLADESH
    elif phone[0] == "7" or phone[:1] == "+7" or phone[0] == "9" or phone[:1] == "+9":
        return PROXY_RU

    return PROXY_US


def add_line_to_file(filename: str, line):
    file = open(filename, "a")
    file.write(f"{line}\n")
    file.close()


def get_account_data(phone):
    with open("db.json", "r") as dbfile:
        data = json.load(dbfile)
        for account_data in data["users"].values():
            if account_data["account"] == phone:
                return account_data


def process_account(phone, host, group_public_link, group_private_link, main_channel_public_link, main_channel_private_link,
                    mirror_channel_public_link, mirror_channel_private_link, main_channel_id, mirror_channel_id, set_names=False, username="", full_name=" ", bio=""):
    if set_names:
        account_data = {
            "account": phone,
            "username": username,
            "first_name": full_name.split(" ")[0],
            "last_name": " ".join(full_name.split(' ')[1:]),
            "bio": bio
        }
    else:
        account_data = get_account_data(phone)
    print(account_data)
    print(phone)
    if os.path.exists(f"json/{host}/{phone}.json"):
        return
    client = TelegramClient(f"hosts/{host}/{phone}.session",
                            API_ID, API_HASH, proxy=get_proxy(phone))
    try:
        client.connect()
        print('connected')
        for dialog in client.iter_dialogs():
            if host not in dialog.title:
                try:
                    dialog.delete()
                    sleep(0.5)
                    print("good")
                except Exception as err:
                    add_line_to_file(
                        f"logs/{host}.out", f"ERROR: COULD NOT LEAVE {dialog.title} FOR PROFILE {phone} -- {err}")

        try:
            client(JoinChannelRequest(group_public_link))
            print('joined')
            time.sleep(random.random() + 1)
        except Exception as err:
            add_line_to_file(f"logs/{host}.out",
                             f"{phone} DID NOT JOIN GROUP {err}")
        try:
            client(JoinChannelRequest(main_channel_public_link))
            print('joined')
            time.sleep(random.random() + 1.5)
        except Exception as err:
            add_line_to_file(f"logs/{host}.out",
                             f"{phone} DID NOT JOIN CHANNEL {err}")
        try:
            client(JoinChannelRequest(mirror_channel_public_link))
            print('joined')
            time.sleep(random.random() + 1)
        except Exception as err:
            add_line_to_file(f"logs/{host}.out",
                             f"{phone} DID NOT JOIN MIRROR {err}")

        if set_names:
            try:
                client(UpdateProfileRequest(
                    first_name=full_name.split(' ')[0],
                    last_name=" ".join(full_name.split(' ')[1:]),
                    about=bio
                ))
            except Exception as err:
                add_line_to_file(
                    f"logs/{host}.out", f"{phone} COULD NOT UPDATE NAME FOR PROFILE {phone} -- {err}")
            try:
                client(UpdateUsernameRequest(username))
            except Exception as err:
                add_line_to_file(
                    f"logs/{host}.out", f"{phone} COULD NOT UPDATE USERNAME FOR PROFILE -- {err}")

        try:
            client(DeletePhotosRequest(client.get_profile_photos('me')))
            print('deleted photos')
        except:
            pass

        with open(f"json/{host}/{phone}.json", "w") as datafile:
            json.dump({
                "number": phone,
                "file_path": f"./accounts/P{phone}",
                "username": account_data["username"] if "username" in account_data else "",
                "first_name": account_data["first_name"] if "first_name" in account_data else "",
                "last_name": account_data["last_name"] if "last_name" in account_data else "",
                "bio": "",
                "hostname": host,
                "group_link": group_private_link,
                "subscribe_channel_id": main_channel_private_link,
                "unsubscribe_channel_id": mirror_channel_private_link,
                "main_id": main_channel_id,
                "mirror_id": mirror_channel_id
            }, datafile, indent=4)

        time.sleep(random.random() + 2)
        client.disconnect()
    except Exception as e:
        os.system(
            f"cp 'hosts/{host}/{phone}.session' 'deleted/{host}/{phone}.session'")
        os.remove(f"hosts/{host}/{phone}.session")
        print(e)


def prepare_host(host):
    try:
        os.mkdir("json")
    except:
        pass
    os.system(f"rm -rf 'hosts/{host}/*.session-journal'")
    try:
        os.mkdir(f"json/{host}")
    except:
        pass
    try:
        os.mkdir("logs")
    except:
        pass
    try:
        os.mkdir("deleted")
    except:
        pass
    try:
        os.mkdir(f"deleted/{host}")
    except:
        pass
