from seleniumwire import webdriver
import time
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import telethon_get_code
import random
import json
import os

from functions import do_logging


def reg(driver, num):
    try:
        driver.get("https://web.telegram.org/k/")
        time.sleep(random.random() * 2 + 15)
    except:
        do_logging([f"{num} could not load Telegram page."])
        return False

    try:
        WebDriverWait(driver, 15).until(
            EC.element_to_be_clickable(
                (
                    By.XPATH,
                    '//button[@class="btn-primary btn-secondary btn-primary-transparent primary rp"]',
                )
            )
        ).click()
        time.sleep(random.random() * 2 + 20)
    except:
        do_logging([f"{num} could not find first button."])
        return False

    try:
        WebDriverWait(driver, 15).until(
            EC.visibility_of_element_located(
                (By.XPATH,
                 '//div[@class="input-field input-field-phone"]/div[1]')
            )
        ).clear()
        WebDriverWait(driver, 15).until(
            EC.visibility_of_element_located(
                (By.XPATH,
                 '//div[@class="input-field input-field-phone"]/div[1]')
            )
        ).send_keys(f"{num}")
        WebDriverWait(driver, 15).until(
            EC.visibility_of_element_located(
                (By.XPATH,
                 '//div[@class="input-field input-field-phone"]/div[1]')
            )
        ).send_keys(Keys.ENTER)
    except:
        do_logging([f"{num} could not request code."])
        return False

    time.sleep(random.random() * 2 + 60)

    try:
        get_code = telethon_get_code.main(num)
        if get_code == "err":
            do_logging([f"{num} could not get code."])
    except:
        do_logging([f"{num} could not get code."])
        return False

    time.sleep(random.random() * 2 + 5)

    try:
        WebDriverWait(driver, 15).until(
            EC.visibility_of_element_located(
                (By.XPATH, '//input[@type="tel"]'))
        ).send_keys(get_code)
    except:
        do_logging([f"{num} could not paste code."])
        return False
    time.sleep(random.random() * 2 + 5)
    return True


def get_numbers(host):
    return [file
            for file in os.listdir(f"json/{host}") if file[-5:] == ".json"]


def register(hosts):
    with open("free_proxies.txt", "r") as proxyfile:
        proxies = [line.strip() for line in proxyfile.readlines()][:-1]
        proxies = [
            f"{':'.join(proxy1.split(':')[2:])}@{':'.join(proxy1.split(':')[:2])}" for proxy1 in proxies]

    for jsonfolder in os.listdir("json"):
        for jsonfile in os.listdir(f"json/{jsonfolder}"):
            data = json.load(open(f"json/{jsonfolder}/{jsonfile}", "r"))
            try:
                if "proxy" in data:
                    proxies.remove(data["proxy"])
            except:
                pass

    for host in hosts:
        print(host)
        proxy_idx = 0
        for jsonfilename in get_numbers(host):
            data = json.load(open(f"json/{host}/{jsonfilename}", "r"))
            if os.path.exists(f"/root/.config/google-chrome/accounts/{host}/{data['number']}"):
                continue
            data["proxy"] = proxies[proxy_idx]
            data["file_path"] = f"/root/.config/google-chrome/accounts/{host}/{data['number']}"
            with open(f"json/{host}/{jsonfilename}", "w") as jsonfile:
                json.dump(data, jsonfile, indent=4)
            try:
                options = Options()
                paste_proxy = data["proxy"]

                proxy_https = {
                    "proxy": {
                        "http": f"http://{paste_proxy}",
                        "https": f"https://{paste_proxy}",
                        "no_proxy": "localhost,127.0.0.1",
                    }
                }
                options.add_argument("--no-sandbox")
                options.add_argument(f"user-data-dir={data['file_path']}")
                # options.headless = True
                driver = webdriver.Chrome(
                    executable_path="driver/chromedriver",
                    chrome_options=options,
                    seleniumwire_options=proxy_https,
                )
                print(data["proxy"])

                proxy_idx += 1
                if not reg(driver, data["number"]):
                    raise Exception()
            except:
                print('removing...')
                try:
                    with open(f"json/{host}/{jsonfilename}", "w") as jsonfile:
                        del data["proxy"]
                        json.dump(data, jsonfile, indent=4)
                    os.system(f"sudo rm -rf '{data['file_path']}'")
                except:
                    print(data['file_path'])

            driver.quit()
