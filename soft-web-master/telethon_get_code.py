from telethon import TelegramClient


api_id = 175132
api_hash = '2016997c709c95fc5577b49ea9a67be3'


def main(phone):
    client = TelegramClient(
        f'sessions1/{phone}.session', api_id, api_hash)
    client.connect()
    if not client.is_user_authorized():
        print('not authorized')
        return 'err'
    else:
        try:
            telegram_messages = client.get_messages(777000, 1)
            start = str(telegram_messages[0]).find('message=')
            code = str(telegram_messages[0])[start:].split(':')[-1].strip()[:5]
            return code
        except Exception as err:
            print(err)
            return 'err'
