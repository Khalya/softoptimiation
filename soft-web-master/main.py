from time import sleep, time
from events import *
from config import *
from database_functions import Database
from functions import do_logging
import sys
import os

HOST = sys.argv[1]
db = Database(HOST)


def execute_event(account, host, event_str, event_data):
    event_info = event_str.split()
    print(event_info)
    if event_info[0] == "join":
        os.system(
            f"""screen -dmS join_{account} bash -c 'source venv/bin/activate; python3.9 selenium_main.py {account} "{host}" join'""")
        os.system('screen -ls')
    elif event_info[0] == "leave":
        os.system(
            f"""screen -dmS leave_{account} bash -c 'source venv/bin/activate; python3.9 selenium_main.py {account} "{host}" leave'""")
        os.system('screen -ls')
    elif event_info[0] == "online":
        os.system(
            f"""screen -dmS online_{account} bash -c 'source venv/bin/activate; python3.9 selenium_main.py {account} "{host}" online'""")
        os.system('screen -ls')
    elif event_info[0] == "change_profile":
        os.system(
            f"""screen -dmS change_profile_{account} bash -c 'source venv/bin/activate; python3.9 selenium_main.py {account} "{host}" change_profile {' '.join(event_data)}'""")
    elif event_info[0] == "block":
        os.system(
            f"""screen -dmS block_{account} bash -c 'source venv/bin/activate; python3.9 selenium_main.py {account} "{host}" block {event_data}'""")
    else:
        raise ValueError("Unidentified event type")


def fire_events():
    print("Looking for events...")
    events = db.pop_events(1)
    for event in events:
        print(event)
        try:
            do_logging([
                f"[{event['account']}] Executing {event['event']} (TS = {event['timestamp']}, NOW = {time()})"])
            execute_event(event["account"], event["host"], event["event"],
                          event["data"] if "data" in event else {})
        except Exception as err:
            print(err)



if __name__ == "__main__":
    while True:
        try:
            fire_events()
        except Exception as err:
            print(err)
        sleep(5)
