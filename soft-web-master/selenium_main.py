from audioop import add
from turtle import down
from seleniumwire import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from random import random
from time import sleep
import pyautogui
import json
import sys
import os
from database_functions import Database
from functions import do_logging


def get_driver(phone, host):
    try:
        f = open(f"json/{host}/{phone}.json")
        LOGIN_INFO = json.load(f)

        options = Options()

        proxy_https = {
            "proxy": {
                "http": f'http://{LOGIN_INFO["proxy"]}',
                "https": f'https://{LOGIN_INFO["proxy"]}',
                "no_proxy": "localhost,127.0.0.1",
            }
        }
        options.add_argument("--no-sandbox")
        options.add_argument("-profile")
        options.add_argument(LOGIN_INFO['file_path'])

        driver = webdriver.Firefox(
            executable_path="driver/geckodriver",
            options=options,
            seleniumwire_options=proxy_https,
        )
        sleep(random() + 2)
        driver.get("https://web.telegram.org/k/")
        driver.maximize_window()
        sleep(random() + 10)
        return driver
    except:
        do_logging([f"[ERROR] {phone} could not build driver."])
        return None


# forward
def forward_msg(driver, phone, forward_group, chat_id, count_msg):
    if count_msg > 1:
        WebDriverWait(driver, 20).until(
            EC.element_to_be_clickable(
                (By.XPATH, f'//li[@data-peer-id="{chat_id}"]'))
        ).click()
        sleep(random())
        msg = WebDriverWait(driver, 10).until(
            EC.presence_of_all_elements_located(
                (By.XPATH, '//div[@class="bubbles-date-group"]/div')
            )
        )
        unread_msg_on = []
        for i in msg:
            if "is-first-unread" in i.get_attribute("outerHTML"):
                data_mid = i.get_attribute("data-mid")
                unread_msg_on.append("1")
                WebDriverWait(driver, 20).until(
                    EC.element_to_be_clickable(
                        (
                            By.XPATH,
                            f'//div[@data-mid="{data_mid}"]/div[1]/div[1]/div[1]/span[1]/div[1]',
                        )
                    )
                ).click()
            elif len(unread_msg_on) > 0:
                data_mid = i.get_attribute("data-mid")
                WebDriverWait(driver, 20).until(
                    EC.element_to_be_clickable(
                        (
                            By.XPATH,
                            f'//div[@data-mid="{data_mid}"]/div[1]/div[1]/div[1]/span[1]/div[1]',
                        )
                    )
                ).click()

        WebDriverWait(driver, 20).until(
            EC.element_to_be_clickable(
                (
                    By.XPATH,
                    '//button[@class="btn-primary btn-transparent text-bold selection-container-forward tgico-forward rp"]',
                )
            )
        ).click()

        WebDriverWait(driver, 20).until(
            EC.visibility_of_element_located(
                (By.XPATH, '//div[@class="popup-title"]/input')
            )
        ).send_keys(forward_group)
        sleep(random())
        WebDriverWait(driver, 20).until(
            EC.element_to_be_clickable(
                (By.XPATH, '//div[@class="popup-body"]/div/div/div/ul/li')
            )
        ).click()

        WebDriverWait(driver, 20).until(
            EC.visibility_of_element_located(
                (
                    By.XPATH,
                    '//div[@class="chat tabs-tab type-chat is-helper-active active"]/div[4]/div[1]/div[1]/div[7]/div[1]/div[1]',
                )
            )
        ).send_keys(f"{phone}, {chat_id} ?@8A;0;(-0):")
        sleep(random() + 3.5)
        driver.execute_script(
            "document.querySelector('button.btn-icon.tgico-none.btn-circle.z-depth-1.btn-send.animated-button-icon.rp.send').click()"
        )
    elif count_msg == 1:
        WebDriverWait(driver, 20).until(
            EC.element_to_be_clickable(
                (By.XPATH, f'//li[@data-peer-id="{chat_id}"]'))
        ).click()
        sleep(random())
        try:
            msg_last = WebDriverWait(driver, 10).until(
                EC.presence_of_all_elements_located(
                    (By.XPATH, '//div[@class="bubbles-date-group"]/div')
                )
            )[-1]
            data_mid = msg_last.get_attribute("data-mid")

            WebDriverWait(driver, 20).until(
                EC.element_to_be_clickable(
                    (
                        By.XPATH,
                        f'//div[@data-mid="{data_mid}"]/div[1]/div[1]/div[1]/span[1]/div[1]',
                    )
                )
            ).click()

            WebDriverWait(driver, 20).until(
                EC.element_to_be_clickable(
                    (
                        By.XPATH,
                        '//button[@class="btn-primary btn-transparent text-bold selection-container-forward tgico-forward rp"]',
                    )
                )
            ).click()

            WebDriverWait(driver, 20).until(
                EC.visibility_of_element_located(
                    (By.XPATH, '//div[@class="popup-title"]/input')
                )
            ).send_keys(forward_group)
            sleep(random())
            WebDriverWait(driver, 20).until(
                EC.element_to_be_clickable(
                    (By.XPATH, '//div[@class="popup-body"]/div/div/div/ul/li')
                )
            ).click()
            WebDriverWait(driver, 20).until(
                EC.visibility_of_element_located(
                    (
                        By.XPATH,
                        '//div[@class="chat tabs-tab type-chat is-helper-active active"]/div[4]/div[1]/div[1]/div[7]/div[1]/div[1]',
                    )
                )
            ).send_keys(f"{phone}, {chat_id} ?@8A;0;(-0):")
            sleep(random() + 2)
            driver.execute_script(
                "document.querySelector('button.btn-icon.tgico-none.btn-circle.z-depth-1.btn-send.animated-button-icon.rp.send').click()"
            )
        except:
            pass


def watch_channels(driver, phone):
    c_id = WebDriverWait(driver, 10).until(
        EC.presence_of_all_elements_located((By.XPATH, "//li"))
    )

    for i in c_id:
        if (
            "-" not in i.get_attribute("data-peer-id")
            or i.get_attribute("data-peer-id") == "777000"
        ):
            continue

        if "unread" in i.get_attribute("innerHTML"):
            try:
                chat_id = i.get_attribute("data-peer-id")
                WebDriverWait(driver, 20).until(
                    EC.element_to_be_clickable(
                        (By.XPATH, f'//li[@data-peer-id="{chat_id}"]')
                    )
                ).click()
                sleep(random())
                print(i.get_attribute("data-peer-id"))

                try:
                    WebDriverWait(driver, 10).until(
                        EC.element_to_be_clickable(
                            (
                                By.XPATH,
                                '//div[@class="chat tabs-tab active type-chat"]/div[4]/div[1]/button[1]',
                            )
                        )
                    ).click()
                except:
                    pass
            except:
                do_logging(
                    [f"[ERROR] {phone} could not watch channel with ID {chat_id}"]
                )

            sleep(random() + 1)


def forward_messages(driver, phone, forward_group):
    x_id = WebDriverWait(driver, 10).until(
        EC.presence_of_all_elements_located((By.XPATH, "//li"))
    )

    for i in x_id:
        if i.get_attribute("data-peer-id") == "777000" or "-" in i.get_attribute(
            "data-peer-id"
        ):
            continue

        if "unread" in i.get_attribute("innerHTML"):
            try:
                int_msg = int(
                    i.find_element_by_xpath(
                        'div[@class="user-caption"]/p[2]/div').text
                )
                chat_id = i.get_attribute("data-peer-id")
                forward_msg(driver, phone, forward_group, chat_id, int_msg)
                sleep(random())
            except:
                do_logging(
                    [f"[ERROR] {phone} could not forward messages from {chat_id}"]
                )


def go_online(phone, host):
    print(f"{phone} [online] start")
    driver = get_driver(phone, host)
    if driver is None:
        do_logging([f"[ERROR] {phone} Could not connect to session"])
        return
    print(f"{phone} [online] driver good")
    try:
        watch_channels(driver, phone)
    except:
        do_logging([f"[ERROR] {phone} Could not watch channels"])
        return
    print(f"{phone} [online] watching channels - good")
    f = open(f"json/{host}/{phone}.json")
    forward_group = json.load(f)["hostname"]
    try:
        forward_messages(driver, phone, forward_group)
    except:
        do_logging([f"[ERROR] {phone} Could not forward messages"])
        return
    print(f"{phone} [online] forwarding messages - good")
    do_logging([f"[SUCCESS] {phone} went online"])
    driver.quit()


def subscribe(phone, host, database):
    try:
        driver = get_driver(phone, host)
        f = open(f"json/{host}/{phone}.json")
        channel = json.load(f)["main_id"]
        try:
            WebDriverWait(driver, 20).until(
                EC.element_to_be_clickable(
                    (By.XPATH, f'//li[@data-peer-id="{channel}"]')
                )
            ).click()
        except:
            do_logging(
                [f"[ERROR] {phone} could not find main subscription channel"])

        sleep(random() + 1)
        msg_subscribe = WebDriverWait(driver, 10).until(
            EC.presence_of_all_elements_located(
                (By.XPATH, '//div[@class="message"]'))
        )

        if 'href="' in msg_subscribe[-1].get_attribute("innerHTML"):
            link_ref = msg_subscribe[-1].find_elements(By.XPATH, "a")[0]
            link = link_ref.text
            link_ref.click()
            sleep(random() + 3.5)

            try:
                join_button = WebDriverWait(driver, 5).until(
                    EC.element_to_be_clickable(
                        (
                            By.XPATH,
                            '/html/body/div[5]/div/div[2]/button[1]'
                        )
                    )
                )
            except:
                join_button = WebDriverWait(driver, 5).until(
                    EC.element_to_be_clickable(
                        (
                            By.XPATH,
                            '/html/body/div[4]/div/div[2]/button[1]'
                        )
                    )
                )
            finally:
                join_button.click()

            do_logging([f"[SUCCESS] {phone} subscribed to {link}"])
            try:
                database.add_user_to_channel(link, phone)
            except Exception as err:
                print(err)
        else:
            do_logging(
                [
                    f"[ERROR] {phone} could not find subscription link in the main channel"
                ]
            )
    except Exception as err:
        print(err)
        do_logging([f"[ERROR] {phone} could not subscribe"])
    sleep(random() + 3)
    driver.quit()


# unsubscribe
def unsubscribe(phone, host, database):
    driver = get_driver(phone, host)
    try:
        f = open(f"json/{host}/{phone}.json")
        channel = json.load(f)["mirror_id"]
        WebDriverWait(driver, 20).until(
            EC.element_to_be_clickable(
                (By.XPATH, f'//li[@data-peer-id="{channel}"]'))
        ).click()
    except:
        do_logging(
            [f"[ERROR] {phone} could not find mirror subscription channel"])

    msg_subsctibe = WebDriverWait(driver, 10).until(
        EC.presence_of_all_elements_located(
            (By.XPATH, '//div[@class="message"]'))
    )

    if 'href="' in msg_subsctibe[-1].get_attribute("innerHTML"):
        for link_idx in range(len(msg_subsctibe[-1].find_elements(By.XPATH, "a"))):
            sleep(random() + 1)
            link_ref = msg_subsctibe[-1].find_elements(By.XPATH, "a")[link_idx]
            link = link_ref.text
            link_ref.click()
            sleep(random() + 2)
            WebDriverWait(driver, 20).until(
                EC.element_to_be_clickable(
                    (
                        By.XPATH,
                        '//div[@class="chat tabs-tab type-chat active"]/div[2]/div[1]/div[2]/div[2]',
                    )
                )
            ).click()
            sleep(random() + 1)
            WebDriverWait(driver, 20).until(
                EC.element_to_be_clickable(
                    (
                        By.XPATH,
                        '//div[@class="chat tabs-tab type-chat active"]/div[2]/div[1]/div[2]/div[2]/div[3]/div[15]',
                    )
                )
            ).click()
            sleep(random() + 1)
            WebDriverWait(driver, 20).until(
                EC.element_to_be_clickable(
                    (By.XPATH, '//button[@class="btn danger rp"]')
                )
            ).click()
            try:
                database.remove_user_from_channel(link, phone)
            except:
                pass
            do_logging([f"[SUCCESS] {phone} unsubscribed from {link}"])
    else:
        do_logging(
            [f"[ERROR] {phone} could not find subscription links in the mirror channel"]
        )

    sleep(random() + 3)
    driver.quit()


def add_photo(phone, host):
    driver = get_driver(phone, host)
    WebDriverWait(driver, 20).until(
        EC.element_to_be_clickable(
            (
                By.XPATH,
                '//div[@class="btn-icon btn-menu-toggle rp sidebar-tools-button is-visible"]',
            )
        )
    ).click()

    WebDriverWait(driver, 20).until(
        EC.element_to_be_clickable(
            (
                By.XPATH,
                '//div[@class="btn-icon btn-menu-toggle rp sidebar-tools-button is-visible menu-open"]/div[3]/div[4]',
            )
        )
    ).click()

    sleep(random() + 3)
    WebDriverWait(driver, 20).until(
        EC.element_to_be_clickable(
            (By.XPATH, '//div[@class="profile-buttons"]/button[1]')
        )
    ).click()
    sleep(random() + 1)
    WebDriverWait(driver, 20).until(
        EC.visibility_of_element_located(
            (
                By.XPATH,
                '//*[@id="column-left"]/div/div[3]/div[2]/div/div[1]/div[1]/div/div[1]/span',
            )
        )
    ).click()

    sleep(random() + 5)
    pyautogui.press("down")
    sleep(random() + .5)
    pyautogui.press("enter")
    sleep(random() + 2)

    WebDriverWait(driver, 20).until(
        EC.element_to_be_clickable(
            (
                By.XPATH,
                '/html/body/div[4]/div/button'
            )
        )
    ).click()

    sleep(random() + 1)
    WebDriverWait(driver, 20).until(
        EC.element_to_be_clickable(
            (
                By.XPATH,
                '//*[@id="column-left"]/div/div[3]/div[2]/button'
            )
        )
    ).click()

    sleep(random() + 5)
    driver.quit()


def change_profile(phone, host, name="", lastname="", bio="", username=""):
    driver = get_driver(phone, host)
    WebDriverWait(driver, 20).until(
        EC.element_to_be_clickable(
            (
                By.XPATH,
                '//div[@class="btn-icon btn-menu-toggle rp sidebar-tools-button is-visible"]',
            )
        )
    ).click()

    WebDriverWait(driver, 20).until(
        EC.element_to_be_clickable(
            (
                By.XPATH,
                '//div[@class="btn-icon btn-menu-toggle rp sidebar-tools-button is-visible menu-open"]/div[3]/div[4]',
            )
        )
    ).click()
    sleep(random() + 3)
    WebDriverWait(driver, 20).until(
        EC.element_to_be_clickable(
            (By.XPATH, '//div[@class="profile-buttons"]/button[1]')
        )
    ).click()

    sleep(random() + 2)

    if name:
        name_field = WebDriverWait(driver, 20).until(
            EC.visibility_of_element_located(
                (
                    By.XPATH,
                    '//div[@class="tabs-tab sidebar-slider-item edit-profile-container active"]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]',
                )
            )
        )
        name_field.clear()
        name_field.send_keys(name)

        sleep(random() + 2)

    if lastname:
        lastname_field = WebDriverWait(driver, 20).until(
            EC.visibility_of_element_located(
                (
                    By.XPATH,
                    '//div[@class="tabs-tab sidebar-slider-item edit-profile-container active"]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]',
                )
            )
        )
        lastname_field.clear()
        lastname_field.send_keys(lastname)

        sleep(random() + 2)
    if bio:
        bio_field = WebDriverWait(driver, 20).until(
            EC.visibility_of_element_located(
                (
                    By.XPATH,
                    '//div[@class="tabs-tab sidebar-slider-item edit-profile-container active"]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[3]/div[1]',
                )
            )
        )
        bio_field.clear()
        bio_field.send_keys(bio)

        sleep(random() + 4)

    try:
        WebDriverWait(driver, 2).until(
            EC.element_to_be_clickable(
                (
                    By.XPATH,
                    '//div[@class="tabs-tab sidebar-slider-item edit-profile-container active"]/div[2]/button[1]',
                )
            )
        ).click()
    except:
        pass

    try:
        sleep(random() + 4)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable(
                (By.XPATH, '//div[@class="profile-buttons"]/button[1]')
            )
        ).click()

        sleep(random() + 3)
    except:
        pass

    if username:
        username_field = WebDriverWait(driver, 20).until(
            EC.visibility_of_element_located(
                (
                    By.XPATH,
                    '//div[@class="tabs-tab sidebar-slider-item edit-profile-container active"]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/input[1]',
                )
            )
        )
        username_field.clear()
        username_field.send_keys(username)

        sleep(random() + 5)

    try:
        confirm_button = WebDriverWait(driver, 5).until(
            EC.element_to_be_clickable(
                (
                    By.XPATH,
                    '//div[@class="tabs-tab sidebar-slider-item edit-profile-container active"]/div[2]/button[1]',
                )
            )
        )
        confirm_button.click()
        sleep(random() + 1)
    except:
        pass

    do_logging([f"[SUCCESS] {phone} changed account info"])

    driver.quit()


def answer_user(phone, host, chat_id, msg_for_user):
    driver = get_driver(phone, host)
    try:
        WebDriverWait(driver, 20).until(
            EC.element_to_be_clickable(
                (By.XPATH, f'//li[@data-peer-id="{chat_id,}"]'))
        ).click()
        WebDriverWait(driver, 20).until(
            EC.visibility_of_element_located(
                (
                    By.XPATH,
                    '//div[@class="chat tabs-tab active type-chat"]/div[4]/div[1]/div[1]/div[7]/div[1]/div[1]',
                )
            )
        ).send_keys(f"{msg_for_user}")
        sleep(random() + 3)
        driver.execute_script(
            "document.querySelector('button.btn-icon.tgico-none.btn-circle.z-depth-1.btn-send.animated-button-icon.rp.send').click()"
        )
    except Exception as err:
        do_logging([f"[FAIL] {phone} answered {chat_id}, error: {err}"])
    driver.quit()
    do_logging([f"[SUCCESS] {phone} answered {chat_id}"])


def block_user(phone, host, chat_id):
    driver = get_driver(phone, host)
    try:
        WebDriverWait(driver, 20).until(
            EC.element_to_be_clickable(
                (By.XPATH, f'//li[@data-peer-id="{chat_id,}"]'))
        ).click()
        WebDriverWait(driver, 20).until(
            EC.element_to_be_clickable(
                (
                    By.XPATH,
                    '//div[@class="chat tabs-tab active type-chat"]/div[2]/div[2]/div[2]',
                )
            )
        ).click()
        WebDriverWait(driver, 20).until(
            EC.element_to_be_clickable(
                (
                    By.XPATH,
                    '//div[@class="chat tabs-tab active type-chat"]/div[2]/div[2]/div[2]/div[3]/div[9]',
                )
            )
        ).click()
        sleep(random() + 3)
        WebDriverWait(driver, 20).until(
            EC.element_to_be_clickable(
                (By.XPATH, '//button[@class="btn danger rp"]'))
        ).click()
    except Exception as err:
        do_logging([f"[FAIL] {phone} blocked {chat_id}, error: {err}"])
    driver.quit()
    do_logging([f"[SUCCESS] {phone} blocked {chat_id}"])


if __name__ == "__main__":
    PHONE = sys.argv[1]
    HOST = sys.argv[2]
    ACTION = sys.argv[3]

    f = open(f"json/{HOST}/{PHONE}.json")
    database = Database(json.load(f)["hostname"])

    if ACTION == "join":
        subscribe(PHONE, HOST, database)
    elif ACTION == "leave":
        unsubscribe(PHONE, HOST, database)
    elif ACTION == "online":
        go_online(PHONE, HOST)
    elif ACTION == "change_profile":
        change_profile(PHONE, HOST, *sys.argv[3:7])
    elif ACTION == "answer":
        answer_user(PHONE, HOST, sys.argv[3])
    elif ACTION == "block":
        block_user(PHONE, HOST)
    elif ACTION == "add_photo":
        add_photo(PHONE, HOST)

    os.system(
        f"for session in $(screen -ls | grep -o '[0-9]*\.{PHONE}_{ACTION}'); do screen -S \"${{session}}\" -X quit; done"
    )
